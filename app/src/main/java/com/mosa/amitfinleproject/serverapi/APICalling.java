package com.mosa.amitfinleproject.serverapi;


import com.mosa.amitfinleproject.serverapi.model.ModelOfListCategory;
import com.mosa.amitfinleproject.serverapi.model.ModelOfListProduct;
import com.mosa.amitfinleproject.serverapi.model.ModelOfProduct;
import com.mosa.amitfinleproject.serverapi.model.ModelOfUser;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APICalling {

    @GET("products")
    Call<ModelOfListProduct> getAllProducts();
    @GET("products")
    Call<ModelOfProduct> getProduct();
    @POST("register")
    Call<ModelOfUser> setUserSignUp(@Body ModelOfUser modelOfUser);
    @POST("login")
    Call<ModelOfUser> getUserLogin(@Body ModelOfUser modelOfUser);
    @GET("categories")
    Call<ModelOfListCategory> getCategories();
    @GET("categories/:{categoryId}")
    Call<ModelOfListProduct> getCategoriesProducts(@Path("categoryId") String id);

    /*@GET("posts")
    public Call<List<ModelOfProduct>> getAllProducts(@Query("userId") String userId);*/
}
