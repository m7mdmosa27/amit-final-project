package com.mosa.amitfinleproject.serverapi.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModelOfListCategory {
    @SerializedName("categories")
    private List<ModelOfCategory> categories;

    public List<ModelOfCategory> getCategories() {
        return categories;
    }

    public void setCategories(List<ModelOfCategory> categories) {
        this.categories = categories;
    }

    public class ModelOfCategory {
        @SerializedName("id")
        private int id;
        @SerializedName("name")
        private String name;
        @SerializedName("avatar")
        private String image_url;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage_url() {
            return image_url;
        }

        public void setImage_url(String image_url) {
            this.image_url = image_url;
        }
    }
}
