package com.mosa.amitfinleproject.serverapi.model;

import com.google.gson.annotations.SerializedName;

public class ModelOfProduct {

    @SerializedName("name")
    private String name;
    @SerializedName("title")
    private String title;
    @SerializedName("description")
    private String description;
    @SerializedName("avatar")
    private String avatar;
    @SerializedName("price")
    private int price;
    @SerializedName("discount")
    private int discount=0;
    @SerializedName("discount_type")
    private String discount_type;
    @SerializedName("currency")
    private String currency = "EGP";
    @SerializedName("in_stock")
    private int in_stock =0;
    private int num_of_product_cart = 1;

    public ModelOfProduct() {
    }

    public ModelOfProduct(String title, String description, String avatar,
                          int price, int num_of_product_cart) {
        this.title = title;
        this.description = description;
        this.avatar = avatar;
        this.price = price;
        this.num_of_product_cart = num_of_product_cart;
    }

    public String getName() {
        return name;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getAvatar() {
        return avatar;
    }

    public int getPrice() {
        return price;
    }

    public int getDiscount() {
        return discount;
    }

    public String getDiscount_type() {
        return discount_type;
    }

    public String getCurrency() {
        return currency;
    }

    public int getIn_stock() {
        return in_stock;
    }

}
