package com.mosa.amitfinleproject.serverapi;

import com.mosa.amitfinleproject.serverapi.model.ModelOfListCategory;
import com.mosa.amitfinleproject.serverapi.model.ModelOfListProduct;
import com.mosa.amitfinleproject.serverapi.model.ModelOfUser;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClint {

    private final static String base_url_link_server = "http://retail.amit-learning.com/api/";
    private static APICalling call;
    private static ApiClint instance;
    String TAG = "api";

    private ApiClint() {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(14, TimeUnit.SECONDS)
                .readTimeout(14,TimeUnit.SECONDS)
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(base_url_link_server)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        call = retrofit.create(APICalling.class);
    }

    // for get instant from Api
    public static ApiClint getInstant(){
        if (instance == null){
            instance = new ApiClint();
            return instance;
        }
        return instance;
    }

    public Call<ModelOfListProduct> getAllProducts(){

        return call.getAllProducts();
    }

    public Call<ModelOfUser> setUserSignUp(ModelOfUser modelOfUser){

        return call.setUserSignUp(modelOfUser);

    }
    public Call<ModelOfUser> getUserLogin(ModelOfUser modelOfUser){

        return call.getUserLogin(modelOfUser);

    }



    public Call<ModelOfListCategory> getCallCategories(){
        return call.getCategories();
    }

    public Call<ModelOfListProduct> getCategoriesProducts(String id){
        return call.getCategoriesProducts(id);
    }






}
