package com.mosa.amitfinleproject.serverapi.model;

public class ModelOfProductCart {
    private String title;
    private String description;
    private String avatar;
    private int price;
    private int num_of_product_cart = 1;

    public ModelOfProductCart() {
    }

    public ModelOfProductCart(String title, String description, String avatar, int price, int num_of_product_cart) {
        this.title = title;
        this.description = description;
        this.avatar = avatar;
        this.price = price;
        this.num_of_product_cart = num_of_product_cart;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getNum_of_product_cart() {
        return num_of_product_cart;
    }

    public void setNum_of_product_cart(int num_of_product_cart) {
        this.num_of_product_cart = num_of_product_cart;
    }
}
