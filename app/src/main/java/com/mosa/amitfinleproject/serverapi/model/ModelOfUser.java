package com.mosa.amitfinleproject.serverapi.model;

import com.google.gson.annotations.SerializedName;

public class ModelOfUser {
    @SerializedName("name")
    private String name;
    @SerializedName("email")
    private String email;
    @SerializedName("password")
    private String pass;
    @SerializedName("token")
    private String token;
    @SerializedName("error")
    private String error;
    @SerializedName("message")
    private String message;

    public void setToken(String token) {
        this.token = token;
    }

    public void setError(String error) {
        this.error = error;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public String getError() {
        return error;
    }

    public String getToken() {
        return token;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}
