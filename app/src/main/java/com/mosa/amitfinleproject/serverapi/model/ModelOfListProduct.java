package com.mosa.amitfinleproject.serverapi.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModelOfListProduct {

    @SerializedName("products")
    private List<ModelOfProduct> products;

    public List<ModelOfProduct> getProducts() {
        return products;
    }

    public void setProducts(List<ModelOfProduct> products) {
        this.products = products;
    }
}
