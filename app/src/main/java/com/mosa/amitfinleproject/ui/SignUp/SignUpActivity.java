package com.mosa.amitfinleproject.ui.SignUp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.mosa.amitfinleproject.MainActivity;
import com.mosa.amitfinleproject.R;
import com.mosa.amitfinleproject.serverapi.ApiClint;
import com.mosa.amitfinleproject.serverapi.model.ModelOfUser;
import com.mosa.amitfinleproject.ui.login.LoginActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {

    EditText txt_name_user, txt_email_user, txt_pass_user;
    Button bt_signup;
    TextView bt_txt_go_login;
    String TAG = "sign";
    ModelOfUser data_back_set;
    FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        mAuth = FirebaseAuth.getInstance();
        txt_email_user = findViewById(R.id.email_user_signup);
        txt_name_user = findViewById(R.id.name_user_signup);
        txt_pass_user = findViewById(R.id.pass_user_signup);
        bt_signup = findViewById(R.id.signup_button);
        bt_txt_go_login = findViewById(R.id.go_to_login);

        setButtonEnable();

        bt_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setUserFirebase(txt_email_user.getText().toString(),
                        txt_pass_user.getText().toString(),
                        txt_name_user.getText().toString());
            }
        });
        bt_txt_go_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
            }
        });
    }
    private void setUserSignUp(Call<ModelOfUser> modelOfUserCall){
        modelOfUserCall.enqueue(new Callback<ModelOfUser>() {
            @Override
            public void onResponse(Call<ModelOfUser> call, Response<ModelOfUser> response) {
                if (response.isSuccessful()){
                    data_back_set = response.body();
                }else {
                    Toast.makeText(SignUpActivity.this, " no data( Field Response )", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ModelOfUser> call, Throwable t) {

            }
        });
    }

    private void setUserFirebase(String user, String pass, String user_name){
        mAuth.createUserWithEmailAndPassword(user,pass)
                .addOnCompleteListener(SignUpActivity.this,
                        new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()){
                                setNameUserFirebase(user_name);
                                startActivity(new Intent(SignUpActivity.this,
                                        MainActivity.class));
                            }else {
                                Log.w(TAG, "signInWithEmail:failure ", task.getException());
                                Toast.makeText(SignUpActivity.this,
                                        task.getException().getLocalizedMessage(),
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
    }

    private void setNameUserFirebase(String user_name){
        FirebaseUser user = mAuth.getCurrentUser();
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(user_name)
                .build();
        if (user != null){
            user.updateProfile(profileUpdates)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Log.d(TAG, "User profile updated.");
                            }else {
                                Log.w(TAG, "not update " + task.getException());
                            }
                        }
                    });
        }
    }

    private void setButtonEnable() {
        TextWatcher watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (txt_pass_user.getText().toString().length() >= 6
                        && !txt_email_user.getText().toString().isEmpty()
                        && !txt_name_user.getText().toString().isEmpty()) {
                    bt_signup.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (txt_pass_user.getText().toString().length() < 6
                        || txt_email_user.getText().toString().isEmpty()
                        || txt_name_user.getText().toString().isEmpty()) {
                    bt_signup.setEnabled(false);
                }
            }
        };

        txt_pass_user.addTextChangedListener(watcher);
        txt_email_user.addTextChangedListener(watcher);
        txt_name_user.addTextChangedListener(watcher);

    }


}