package com.mosa.amitfinleproject.ui.categories;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mosa.amitfinleproject.R;
import com.mosa.amitfinleproject.serverapi.ApiClint;
import com.mosa.amitfinleproject.serverapi.model.ModelOfListCategory;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoriesFragment extends Fragment {

    private DashboardViewModel dashboardViewModel;
    private RecyclerView rc_categories;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        dashboardViewModel =
                ViewModelProviders.of(this).get(DashboardViewModel.class);
        View root = inflater.inflate(R.layout.fragment_categories, container, false);
        rc_categories = root.findViewById(R.id.rc_categories);
        getCategoriesList(ApiClint.getInstant().getCallCategories());
        rc_categories.setLayoutManager(new GridLayoutManager(requireContext(),2,RecyclerView.VERTICAL,false));


        return root;
    }

    public void getCategoriesList( Call<ModelOfListCategory> call){
        call.enqueue(new Callback<ModelOfListCategory>() {
            @Override
            public void onResponse(Call<ModelOfListCategory> call, Response<ModelOfListCategory> response) {
                rc_categories.setAdapter(new AdapterRCCategories(response.body()));
            }

            @Override
            public void onFailure(Call<ModelOfListCategory> call, Throwable t) {

            }
        });
    }

}