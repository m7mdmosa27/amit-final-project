package com.mosa.amitfinleproject.ui.home;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.card.MaterialCardView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mosa.amitfinleproject.R;
import com.mosa.amitfinleproject.serverapi.model.ModelOfProduct;
import com.mosa.amitfinleproject.serverapi.model.ModelOfProductCart;
import com.mosa.amitfinleproject.serverapi.model.ModelOfUser;
import com.mosa.amitfinleproject.ui.productdetails.ProductDetails;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdapterRCHome extends RecyclerView.Adapter<AdapterRCHome.HomeViewHolder> {

    private final List<ModelOfProduct> products;
    private final Context context;
    final static String TAG = "Home";

    public AdapterRCHome(List<ModelOfProduct> products, Context context) {
        this.products = products;
        this.context = context;
    }

    @NonNull
    @Override
    public HomeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new HomeViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_view_item_home,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull HomeViewHolder holder, int position) {
        ModelOfProduct product= products.get(position);
        String st_price = product.getPrice() +" "+ product.getCurrency();

        holder.txt_title_of_product.setText(product.getTitle());
        holder.txt_details_of_product.setText(product.getDescription());
        holder.txt_price_product.setText(st_price);
        Picasso.get().load(product.getAvatar()).into(holder.image_product);
        holder.item_view_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // go to activity details
                Intent i = new Intent(context, ProductDetails.class);
                i.putExtra("title",product.getTitle());
                i.putExtra("description",product.getDescription());
                i.putExtra("image",product.getAvatar());
                i.putExtra("price",product.getPrice());
                i.putExtra("details",product.getDescription());
                context.startActivity(i);
            }
        });
        holder.bt_add_product_to_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "add", Toast.LENGTH_SHORT).show();
                setProductInCart(product.getTitle(), product.getAvatar()
                        , product.getDescription(), product.getPrice());
            } 
        });



    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    private void setProductInCart(String title, String image, String description, int price){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null){
            DatabaseReference myRef = database.getReference("Cart").child(user.getDisplayName());
            ModelOfProductCart product_adding = new ModelOfProductCart(title, description,
                    image, price,1);
            myRef.push().setValue(product_adding);
        }else {
            Log.w(TAG, "Not user ");
            Toast.makeText(context, "user is not exist", Toast.LENGTH_SHORT).show();
        }


    }





    class HomeViewHolder extends RecyclerView.ViewHolder{

        private final ImageView image_product;
        private final ImageButton bt_add_product_to_cart;
        private final TextView txt_title_of_product,txt_details_of_product,txt_price_product;
        private final MaterialCardView item_view_card;
        public HomeViewHolder(@NonNull View view) {
            super(view);
            image_product = view.findViewById(R.id.image_i_rc_home);
            bt_add_product_to_cart =view.findViewById(R.id.bt_add_product_ib);
            txt_title_of_product = view.findViewById(R.id.txt_title_i_rc_home);
            txt_details_of_product = view.findViewById(R.id.txt_details_i_rc_home);
            txt_price_product = view.findViewById(R.id.product_price_tv);
            item_view_card = view.findViewById(R.id.product_iRC_card);
        }
    }
}
