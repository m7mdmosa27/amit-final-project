package com.mosa.amitfinleproject.ui.productdetails;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mosa.amitfinleproject.MainActivity;
import com.mosa.amitfinleproject.R;
import com.mosa.amitfinleproject.serverapi.model.ModelOfProductCart;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ProductDetails extends AppCompatActivity {

    TextView txt_title,txt_details,txt_price;
    ImageView image_item;
    Button bt_add_item_in_cart;
    List<String> details = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detalis);
        txt_title = findViewById(R.id.title_item);
        txt_details = findViewById(R.id.detatils_item);
        txt_price = findViewById(R.id.product_price_details_tv);
        image_item = findViewById(R.id.image_item);
        bt_add_item_in_cart = findViewById(R.id.add_to_cart_btn);
        getDataIntent();
        bt_add_item_in_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setProductInCart(details.get(0),details.get(3),details.get(1),Integer.parseInt(details.get(2)));
                moveTaskToBack(true);
                startActivity(new Intent(ProductDetails.this, MainActivity.class));
            }
        });
    }


    public void getDataIntent(){
        Intent i= getIntent();
        txt_title.setText(i.getStringExtra("title"));
        txt_details.setText(i.getStringExtra("description"));
        txt_price.setText(i.getIntExtra("price",0) + "EGY");
        Picasso.get().load(i.getStringExtra("image")).into(image_item);
        details.clear();
        details.add(i.getStringExtra("title"));
        details.add(i.getStringExtra("description"));
        int s = i.getIntExtra("price",0);
        details.add(String.valueOf(s));
        details.add(i.getStringExtra("image"));

    }

    private void setProductInCart(String title, String image, String description, int price){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null){
            DatabaseReference myRef = database.getReference("Cart").child(user.getDisplayName());
            ModelOfProductCart product_adding = new ModelOfProductCart(title, description,
                    image, price,1);
            myRef.push().setValue(product_adding);
        }else {
            Toast.makeText(ProductDetails.this, "user is not exist", Toast.LENGTH_SHORT).show();
        }


    }
}