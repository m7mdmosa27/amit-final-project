package com.mosa.amitfinleproject.ui.cart;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mosa.amitfinleproject.R;
import com.mosa.amitfinleproject.serverapi.model.ModelOfProductCart;
import com.mosa.amitfinleproject.ui.home.HomeFragment;

import java.util.ArrayList;
import java.util.List;

public class CartFragment extends Fragment {

    private NotificationsViewModel notificationsViewModel;
    private RecyclerView rc_home;
    private Button bt_clear_cart;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        notificationsViewModel =
                ViewModelProviders.of(this).get(NotificationsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_cart, container, false);

        rc_home = root.findViewById(R.id.rc_cart);
        bt_clear_cart = root.findViewById(R.id.bt_clear_cart);

        getProductInCart();
        rc_home.setLayoutManager(new GridLayoutManager(requireContext(),1,RecyclerView.VERTICAL,false));
        rc_home.setItemAnimator(new DefaultItemAnimator());

        bt_clear_cart.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        clearCart();
                        Navigation.findNavController(view).navigate(R.id.action_navigation_cart_self);
                    }
                }
        );


        return root;
    }


    private void getProductInCart(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference("Cart");
        DatabaseReference myRef;
        ValueEventListener eventListener;
        if (user != null){
            myRef = rootRef.child(user.getDisplayName());
            eventListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    List<ModelOfProductCart> modelOfProducts = new ArrayList<>();
                    for(DataSnapshot ds : dataSnapshot.getChildren()) {
                        ModelOfProductCart model = ds.getValue(ModelOfProductCart.class);
                        modelOfProducts.add(model);
                        System.out.println(" "+ model.getAvatar() +" \n "+ model.getDescription() +" \n "+
                                model.getTitle() +" \n "+ model.getPrice());
                    }
                    rc_home.setAdapter(new AdapterRCCart(modelOfProducts,getContext()));
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            };
            myRef.addListenerForSingleValueEvent(eventListener);
        }else {
            Toast.makeText(getActivity(), "user is not exist", Toast.LENGTH_SHORT).show();
        }
    }

    private void clearCart(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference("Cart");
        rootRef.child(user.getDisplayName()).removeValue();

    }


}