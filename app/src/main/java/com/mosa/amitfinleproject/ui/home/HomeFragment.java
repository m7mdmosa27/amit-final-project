package com.mosa.amitfinleproject.ui.home;

import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mosa.amitfinleproject.R;
import com.mosa.amitfinleproject.serverapi.ApiClint;
import com.mosa.amitfinleproject.serverapi.model.ModelOfListProduct;
import com.mosa.amitfinleproject.serverapi.model.ModelOfProduct;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private RecyclerView rc_home;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        rc_home = root.findViewById(R.id.recycler_home_view);
        rc_home.setLayoutManager(new GridLayoutManager(requireContext(),2));
        rc_home.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(12), true));
        rc_home.setItemAnimator(new DefaultItemAnimator());

        ApiClint connect = ApiClint.getInstant();
        getAllProducts(connect.getAllProducts());

        return root;
    }

    private void getAllProducts(Call<ModelOfListProduct> call){

        call.enqueue(new Callback<ModelOfListProduct>() {
            @Override
            public void onResponse(Call<ModelOfListProduct> call, Response<ModelOfListProduct> response) {
                AdapterRCHome adapter = new AdapterRCHome(response.body().getProducts(), getContext());
                rc_home.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<ModelOfListProduct> call, Throwable t) {
                Toast.makeText(requireContext(), "Error happened", Toast.LENGTH_SHORT).show();
                Log.d("Error",t.toString());
            }
        });

    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }


        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }


}