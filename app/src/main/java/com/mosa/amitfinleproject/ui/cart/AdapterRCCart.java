package com.mosa.amitfinleproject.ui.cart;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mosa.amitfinleproject.R;
import com.mosa.amitfinleproject.serverapi.model.ModelOfProduct;
import com.mosa.amitfinleproject.serverapi.model.ModelOfProductCart;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class AdapterRCCart extends RecyclerView.Adapter<AdapterRCCart.CartViewHolder> {

    private List<ModelOfProductCart> products;
    private Context context;

    public AdapterRCCart(List<ModelOfProductCart> products, Context context) {
        this.products = products;
        this.context = context;
    }

    @NonNull
    @Override
    public CartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CartViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cart_item_view, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CartViewHolder holder, int position) {
        ModelOfProductCart product = products.get(position);
        String st_price = product.getPrice() +" EGY";
        if (product.getAvatar() != null){
            Picasso.get().load(product.getAvatar()).into(holder.image_item);
        }
        holder.item_title.setText(product.getTitle());
        holder.item_details.setText(product.getDescription());
        holder.item_price.setText(st_price);
        holder.bt_plus.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int i =Integer.parseInt(holder.item_count.getText().toString());
                        i++;
                        holder.item_count.setText("" + i);
                    }
                }
        );
        holder.bt_minus.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int i =Integer.parseInt(holder.item_count.getText().toString());
                        i--;
                        holder.item_count.setText("" + i);
                    }
                }
        );


    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    class CartViewHolder extends RecyclerView.ViewHolder{
        private final TextView item_title, item_details,item_price,item_count;
        private final ImageButton bt_plus,bt_minus;
        private final ImageView image_item;
        public CartViewHolder(@NonNull View view) {
            super(view);
            item_count = view.findViewById(R.id.txt_counter_in_cart);
            item_title = view.findViewById(R.id.txt_title_in_cart);
            item_details = view.findViewById(R.id.txt_details_in_cart);
            item_price = view.findViewById(R.id.price_in_cart);
            bt_plus = view.findViewById(R.id.bt_plus_item);
            bt_minus = view.findViewById(R.id.bt_minus_in_cart);
            image_item = view.findViewById(R.id.image_item_in_cart);
        }
    }
}
