package com.mosa.amitfinleproject.ui.login;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.internal.$Gson$Preconditions;
import com.mosa.amitfinleproject.MainActivity;
import com.mosa.amitfinleproject.R;
import com.mosa.amitfinleproject.serverapi.ApiClint;
import com.mosa.amitfinleproject.serverapi.model.ModelOfUser;
import com.mosa.amitfinleproject.ui.SignUp.SignUpActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    EditText txt_email_user, txt_pass_user;
    Button bt_login;
    TextView bt_txt_go_signup;
    ModelOfUser data_back_set;
    FirebaseAuth mAuth;
    static String TAG = "login";


    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // initialize firebase Authentication
        mAuth = FirebaseAuth.getInstance();

        txt_email_user = findViewById(R.id.email_user_login);
        txt_pass_user = findViewById(R.id.pass_user_login);
        bt_login = findViewById(R.id.login_button);
        bt_txt_go_signup = findViewById(R.id.go_to_signup);

        setButtonEnable();
        bt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getUserLoginFireBase(txt_email_user.getText().toString(),
                        txt_pass_user.getText().toString());
            }
        });

        bt_txt_go_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
            }
        });

    }

    private void getUserLogin(Call<ModelOfUser> modelOfUserCall) {
        modelOfUserCall.enqueue(new Callback<ModelOfUser>() {
            @Override
            public void onResponse(Call<ModelOfUser> call, Response<ModelOfUser> response) {
                if (response.isSuccessful()) {
                    data_back_set = response.body();
                } else {
                    Toast.makeText(LoginActivity.this, " no data Field response ", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ModelOfUser> call, Throwable t) {

            }
        });
    }

    private void getUserLoginFireBase(String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        } else {
                            Toast.makeText(LoginActivity.this, task.getException().getLocalizedMessage(),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void setButtonEnable() {
        TextWatcher watcher =new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (txt_pass_user.getText().toString().length() >= 6
                        && !txt_email_user.getText().toString().isEmpty()) {
                    bt_login.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (txt_pass_user.getText().toString().length() < 6
                        || txt_email_user.getText().toString().isEmpty()) {
                    bt_login.setEnabled(false);
                }
            }
        };
        txt_pass_user.addTextChangedListener(watcher);
        txt_email_user.addTextChangedListener(watcher);


    }


}