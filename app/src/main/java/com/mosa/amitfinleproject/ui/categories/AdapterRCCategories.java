package com.mosa.amitfinleproject.ui.categories;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.mosa.amitfinleproject.R;
import com.mosa.amitfinleproject.serverapi.model.ModelOfListCategory;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdapterRCCategories extends RecyclerView.Adapter<AdapterRCCategories.ViewHolder> {

    ModelOfListCategory modelOfListCategory;

    public AdapterRCCategories(ModelOfListCategory modelOfListCategory) {
        this.modelOfListCategory = modelOfListCategory;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.categories_item_view,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ModelOfListCategory.ModelOfCategory category = modelOfListCategory.getCategories().get(position);
        holder.name_category.setText(category.getName());
        Picasso.get().load(category.getImage_url()).into(holder.image_category);

        holder.bt_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putInt("category id",category.getId());
                Navigation.findNavController(view).navigate(R.id.action_navigation_categories_to_navigation_home
                        ,bundle);
            }
        });


    }

    @Override
    public int getItemCount() {
        return 6;
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private final TextView name_category;
        private final ImageView image_category;
        private final CardView bt_category;
        public ViewHolder(@NonNull View view) {
            super(view);
            name_category = view.findViewById(R.id.name_category);
            image_category = view.findViewById(R.id.image_item_categories);
            bt_category = view.findViewById(R.id.bt_category);
        }
    }

}
